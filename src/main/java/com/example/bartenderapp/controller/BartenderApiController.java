package com.example.bartenderapp.controller;


import com.example.bartenderapp.domain.dto.*;
import com.example.bartenderapp.domain.repository.DrinkRepository;
import com.example.bartenderapp.service.BartenderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/api")
public class BartenderApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(BartenderApiController.class);


    private final BartenderService bartenderService;

    @Autowired
    public BartenderApiController(BartenderService bartenderService) {
        this.bartenderService = bartenderService;
    }

    @GetMapping(value = "/getDrinks")
    public ResponseEntity<List<DrinkDto>> getDrinks(){

        List<DrinkDto> drinkDto = bartenderService.getAllDrinks();
        return new ResponseEntity<>(drinkDto,HttpStatus.OK);
    }

    @GetMapping(value = "/setId/{nick}")
    public ResponseEntity<IdDto> setId(@PathVariable("nick") String nick){

        return new ResponseEntity<>(bartenderService.setUserId(nick),HttpStatus.OK);
    }

    @GetMapping(value = "/getRange/{id}")
    public ResponseEntity<RangeDto> getRange(@PathVariable("id") long id){

        RangeDto rangeDto = bartenderService.getRange(id);

        if(rangeDto.getRange()<0 || rangeDto.getRange()>2){
            return new ResponseEntity<>(rangeDto,HttpStatus.FORBIDDEN);
        }else{
            return new ResponseEntity<>(rangeDto,HttpStatus.OK);
        }
    }

    @GetMapping(value = "/getUsers")
    public  ResponseEntity<List<UserDto>> getUsers(){
        return new ResponseEntity<>(bartenderService.getUsers(),HttpStatus.OK);
    }

    @PutMapping(value = "/setRange/{id}")
    public ResponseEntity<Void> setRange(@PathVariable("id") long id){
        boolean result = bartenderService.setRange(id);

        if(result){
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/getFavoriteDrink/{id}")
    public ResponseEntity<List<DrinkDto>> getFavoriteDrinks(@PathVariable(value = "id") long id){
        return new ResponseEntity<>(bartenderService.getFavoriteDrink(id), HttpStatus.OK);
    }

    @GetMapping(value = "/getOwnedIngredient/{id}")
    public ResponseEntity<List<IngredientDto>> getOwnedIngredientDto(@PathVariable(value = "id") long id){
        return new ResponseEntity<>(bartenderService.getOwnedIngredient(id),HttpStatus.OK);
    }

    @GetMapping(value = "/getIngredients")
    public ResponseEntity<List<IngredientDto>> getAllIngredients(){
        return new ResponseEntity<>(bartenderService.getIngredients(),HttpStatus.OK);
    }

    @GetMapping(value = "/getTaste")
    public ResponseEntity<List<TasteDto>> getTaste(){
        return new ResponseEntity<>(bartenderService.getTaste(),HttpStatus.OK);
    }

    @GetMapping(value = "/getDrinkRecipe/{id}")
    public ResponseEntity<List<RecipeDto>> getDrinkRecipe(@PathVariable("id") long id){

        return new ResponseEntity<>(bartenderService.getDrinkRecipe(id),HttpStatus.OK);
    }

    @GetMapping(value = "/getDrinksWithOwnedIngredient/{id}")
    public ResponseEntity<List<DrinkDto>> getDrinksWithOwnedIngredient(@PathVariable(value = "id") int id){

        return new ResponseEntity<>(bartenderService.getDrinksWithOwnedIngredient(id),HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteFavoriteDrink")
    public ResponseEntity<Void> deleteFavoriteDrinks(@RequestParam(value = "user_id",required = false) long user_id,
                                                     @RequestParam(value = "drink_id",required = false) long drink_id){

            boolean result = this.bartenderService.deleteFavoriteDrink(user_id,drink_id);

            if(result){
                return new ResponseEntity<>(HttpStatus.OK);
            }else{
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }

    }

    @DeleteMapping(value = "/deleteOwnedIngredient")
    public ResponseEntity<Void> deleteOwnedIngredient(@RequestParam(value = "user_id",required = false) long user_id,
                                                      @RequestParam(value = "ingredient_id",required = false) long ingredient_id) {

        boolean result = this.bartenderService.deleteOwnedIngredient(user_id, ingredient_id);

        if (result) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(value = "/addFavoriteDrink")
    public ResponseEntity<Void> addFavoriteDrink(@RequestParam(value = "user_id",required = false) long user_id,
                                                 @RequestParam(value = "drink_id",required = false) long drink_id){

        boolean result = this.bartenderService.addFavoriteDrink(user_id,drink_id);
        if(result){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(value = "/addOwnedIngredient")
    public ResponseEntity<Void> addOwnedIngredient(@RequestBody OwnedIngredientDto ownedIngredientDto){

        boolean result = this.bartenderService.addOwnedIngredient(ownedIngredientDto);
        if(result){
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(value = "/addIngredient")
    public ResponseEntity<InfoDto> addIngredient(@RequestParam(value = "name",required = false) String name,
                                                 @RequestParam(value = "percent",required = false) int percent,
                                                 @RequestParam(value = "description",required = false) String description){

        boolean result = bartenderService.addIngredient(name,percent,description);
        InfoDto infoDto;

        if(result){
            infoDto = new InfoDto("YOU ADD NEW INGREDIENT");

            return new ResponseEntity<>(infoDto,HttpStatus.OK);
        }else{
            infoDto = new InfoDto("Error");

            return new ResponseEntity<>(infoDto,HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping(value = "/addDrink")
    public ResponseEntity<InfoDto> addDrink(@RequestBody NewDrinkDto newDrinkDto) {
        boolean result  = this.bartenderService.addDrink(newDrinkDto);

        if(result){
            InfoDto infoDto = new InfoDto("IS GOOD");

            return new ResponseEntity<>(infoDto,HttpStatus.OK);
        }else{
            InfoDto infoDto = new InfoDto("Error");

            return new ResponseEntity<>(infoDto,HttpStatus.FORBIDDEN);
        }
    }


}
