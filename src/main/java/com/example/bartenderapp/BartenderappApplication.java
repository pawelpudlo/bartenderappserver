package com.example.bartenderapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BartenderappApplication {

    public static void main(String[] args) {
        SpringApplication.run(BartenderappApplication.class, args);
    }

}
