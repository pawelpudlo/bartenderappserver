package com.example.bartenderapp.domain.mapper;

import com.example.bartenderapp.domain.dto.TasteDto;
import com.example.bartenderapp.domain.entity.Taste;
import com.example.bartenderapp.domain.repository.TasteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TasteMapper {

    private TasteRepository tasteRepository;

    @Autowired
    public TasteMapper(TasteRepository tasteRepository) {
        this.tasteRepository = tasteRepository;
    }

    public List<TasteDto> getListTastDto(){

        List<Taste> tastes = tasteRepository.findAll();
        List<TasteDto> tasteList = new ArrayList<>();

        for(Taste taste:tastes){
            tasteList.add(new TasteDto(taste.getId(),
                    taste.getName()));
        }

        return tasteList;

    }
}
