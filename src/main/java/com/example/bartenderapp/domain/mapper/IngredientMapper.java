package com.example.bartenderapp.domain.mapper;

import com.example.bartenderapp.domain.dto.IdDto;
import com.example.bartenderapp.domain.dto.IngredientDto;
import com.example.bartenderapp.domain.dto.OwnedIngredientDto;
import com.example.bartenderapp.domain.entity.Ingredient;
import com.example.bartenderapp.domain.entity.OwnedAlcohol;
import com.example.bartenderapp.domain.repository.IngredientRepository;
import com.example.bartenderapp.domain.repository.OwnedAlcoholRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class IngredientMapper {

    private IngredientRepository ingredientRepository;
    private OwnedAlcoholRepository ownedAlcoholRepository;

    @Autowired
    public IngredientMapper(IngredientRepository ingredientRepository,
                            OwnedAlcoholRepository ownedAlcoholRepository) {
        this.ingredientRepository = ingredientRepository;
        this.ownedAlcoholRepository = ownedAlcoholRepository;
    }

    public List<IngredientDto> getListWithOwnedIngredientInDto(long id){
        List<Ingredient> ingredients = ingredientRepository.getOwnedIngredientByUserId(id);
        return getIngredientDtoList(ingredients);

    }

    public List<IngredientDto> getAllIngredients(){
        List<Ingredient> ingredients = ingredientRepository.findAll();
        return getIngredientDtoList(ingredients);
    }

    private List<IngredientDto> getIngredientDtoList(List<Ingredient> ingredients){
        List<IngredientDto> ingredientDtoList = new ArrayList<>();

        for(Ingredient ingredient:ingredients){

            ingredientDtoList.add(new IngredientDto(ingredient.getId(),
                    ingredient.getName(),
                    ingredient.getPercent(),
                    ingredient.getDescription()));
        }

        return ingredientDtoList;
    }

    public boolean deleteOwnedIngredient(long user_id,long drink_id){

        try{
            long id = this.ownedAlcoholRepository.getIdOperationOwnedAlcoholByUserIdAndDrinkId(user_id,drink_id);
            this.ownedAlcoholRepository.deleteById(id);
            return true;
        }catch (Exception var4){
            System.out.println(var4.getMessage());
            return false;
        }
    }

    public boolean addOwnedIngredient(OwnedIngredientDto ownedIngredientDto) {

        List<IdDto> ingredientIdForAdd = ownedIngredientDto.getIngredients();
        long userId = ownedIngredientDto.getUserid();
        List<Long> emptyList = new ArrayList<>();

        long numbers;
        long ingredientId;

        try {

            for(IdDto ingredient:ingredientIdForAdd){
                ingredientId = ingredient.getId();
                for (Long el:emptyList) {
                    if(ingredientId == el) return false;
                }
                emptyList.add(ingredientId);
                numbers = this.ownedAlcoholRepository.getNumberOfAppearancesInTable(userId,ingredientId);
                if(numbers!=0) return false;
            }

            try{

                numbers = this.ownedAlcoholRepository.getMaxOperationId();
                numbers++;
            }catch (Exception var4){
                numbers = 1;
            }

            for(IdDto ingredient:ingredientIdForAdd){
                OwnedAlcohol ownedAlcohol = new OwnedAlcohol(numbers,userId,ingredient.getId());
                this.ownedAlcoholRepository.save(ownedAlcohol);
                numbers++;
            }

            return true;
        }catch (Exception var4){
            System.out.println(var4.getMessage());
            return false;
        }
    }

    public boolean addIngredient(String name, int percent, String description) {

        long id;

        try {
            List<Ingredient> ingredients = this.ingredientRepository.findAll();

            if(ingredients.size() == 0){
                id = 1;
            }else{
                for (Ingredient ingredient:ingredients){
                    if(ingredient.getName().toLowerCase().equals(name.toLowerCase())) return false;
                }
                id = this.ingredientRepository.getMaxOperationId();
                id++;
            }




            Ingredient ingredient = new Ingredient(id,name,percent,description);
            this.ingredientRepository.save(ingredient);

            return true;
        }catch (Exception var4){
            System.out.println(var4.getMessage());
            return false;
        }
    }

}
