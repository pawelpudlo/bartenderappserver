package com.example.bartenderapp.domain.mapper;

import com.example.bartenderapp.BartenderappApplication;
import com.example.bartenderapp.domain.dto.DrinkDto;
import com.example.bartenderapp.domain.dto.NewDrinkDto;
import com.example.bartenderapp.domain.dto.NewDrinkRecipeDto;
import com.example.bartenderapp.domain.dto.RecipeDto;
import com.example.bartenderapp.domain.entity.*;
import com.example.bartenderapp.domain.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DrinkListMapper {

    private DrinkTasteRepository drinkTasteRepository;
    private TasteRepository tasteRepository;
    private FavoriteDrinkRepository favoriteDrinkRepository;
    private DrinkRepository drinkRepository;
    private RecipeRepository recipeRepository;
    private IngredientRepository ingredientRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(DrinkListMapper.class);


    @Autowired
    public DrinkListMapper(DrinkTasteRepository drinkTasteRepository,
                           TasteRepository tasteRepository,
                           FavoriteDrinkRepository favoriteDrinkRepository,
                           DrinkRepository drinkRepository,
                           RecipeRepository recipeRepository,
                           IngredientRepository ingredientRepository) {
        this.drinkTasteRepository = drinkTasteRepository;
        this.tasteRepository = tasteRepository;
        this.drinkRepository = drinkRepository;
        this.recipeRepository = recipeRepository;
        this.ingredientRepository = ingredientRepository;
        this.favoriteDrinkRepository = favoriteDrinkRepository;
    }
    /* Zwraca DrinkDto
        Przyjmuje listę encji Drink
        W kodzie petla for chodzi po podanej liście i rozdziela dane by stowrzyć obiekt modelu DrinkDto
        Na końcu dany obiekt jest dodawany do listy obiektów DrinkDto

     */
    public List<DrinkDto> mapToDto(List<Drink> drink){
        List<DrinkDto> drinksDto = new ArrayList<>();

        for(Drink drinks: drink){
            DrinkDto drinkDto = new DrinkDto();

            drinkDto.setId_drink(drinks.getDrinkId());
            drinkDto.setName_drink(drinks.getNameDrink());
            drinkDto.setMethod(drinks.getMethodDrink());
            //Użyto metody getTaste
            drinkDto.setTaste(getTaste(drinks.getDrinkId()));

            drinksDto.add(drinkDto);

        }
        return drinksDto;
    }
    // Zwraca w postaci string smak drinka po podaniu ID drinka
    private String getTaste(Long id){


        Long drinkTasteId = this.drinkTasteRepository.findTasteById(id);


        return this.tasteRepository.findTasteNameById(drinkTasteId);
    }


    /*
    Przymuje id uzytkownika w postaci Longa

        Środek podobny do porzedniej metody

        zwraca Liste z obiektami DrinkDto ulubionych drinków użytkownika

        Wystepuje wyjątek Exception

        Jeżeli metoda z drinkRepository getListFavoriteDrinkByUserId(id) zwróci null wtedy rzucany jest wyjątek

     */
    public List<DrinkDto> getListFavoriteDrink(long id){
        List<DrinkDto> drinksDto = new ArrayList<>();
        try {
            List<Drink> drinks = this.drinkRepository.getListFavoriteDrinkByUserId(id);

            for(Drink drink: drinks){
                DrinkDto drinkDto = new DrinkDto();

                drinkDto.setId_drink(drink.getDrinkId());
                drinkDto.setName_drink(drink.getNameDrink());
                drinkDto.setMethod(drink.getMethodDrink());
                drinkDto.setTaste(getTaste(drink.getDrinkId()));

                drinksDto.add(drinkDto);

            }
            return drinksDto;
        }catch (Exception var4){
            System.out.println(var4.getMessage());
            return drinksDto;
        }

    }


    /*
        Przyjmuje id danego drinku w postaci LONG

        this.recipeRepository.getDrinkRecipe(id); zwraca Liste enji Recipe z receptą drinka

        W wyjątku


     */

    public List<RecipeDto> getDrinkRecipe(long id){
        List<RecipeDto> recipeDtoList = new ArrayList<>();

        List<Recipe> drinkRecipe = this.recipeRepository.getDrinkRecipe(id);

        for (Recipe recipe:drinkRecipe){
            //long ingredientId = recipe.getIngredientId(); zmienna przyjmuje id  danego składnika drinku
            long ingredientId = recipe.getIngredientId();

            try{
                Ingredient drinkIngredient= this.ingredientRepository.getDrinkIngredientByDrinkId(ingredientId);
                String ingredient_name = drinkIngredient.getName();
                String proportion = recipe.getProportion();
                int perecent = drinkIngredient.getPercent();

                recipeDtoList.add(new RecipeDto(ingredient_name,proportion,perecent));
            }catch (Exception var4){
                System.out.println(var4.getMessage());
            }

        }

        return recipeDtoList;
    }

    public List<DrinkDto> getDrinkWithOwnedIngredientInDto(long id){
        List<DrinkDto> drinksDto = new ArrayList<>();
        try {
            List<Drink> drinks = this.drinkRepository.getListDrinkWithOwnedIngredients(id);
            drinks = drinks.stream().distinct().collect(Collectors.toList());
            for(Drink drink: drinks){
                DrinkDto drinkDto = new DrinkDto();

                drinkDto.setId_drink(drink.getDrinkId());
                drinkDto.setName_drink(drink.getNameDrink());
                drinkDto.setMethod(drink.getMethodDrink());
                drinkDto.setTaste(getTaste(drink.getDrinkId()));

                drinksDto.add(drinkDto);

            }
        }catch (Exception var4){
            System.out.println(var4.getMessage());
        }

        return drinksDto;

    }

    public boolean deleteFavoriteDrink(long user_id,long drink_id){

        try{
            long id = this.favoriteDrinkRepository.getIdOperationFavoriteDrinkByUserIdAndDrinkId(user_id,drink_id);
            this.favoriteDrinkRepository.deleteById(id);
            return true;
        }catch (Exception var4){
            System.out.println(var4.getMessage());
            return false;
        }

    }

    public boolean addFavoriteDrink(long user_id, long drink_id){
        try{

            long numbers = this.favoriteDrinkRepository.getNumberOfAppearancesInTable(user_id,drink_id);

            if(numbers !=0) return false;

            long id;
            try {
                id = this.favoriteDrinkRepository.getMaxOperationId();
                id++;
            }catch (Exception var4){
                id = 1;
            }


            FavoriteDrink newFavoriteDrink = new FavoriteDrink(id,user_id,drink_id);


            this.favoriteDrinkRepository.save(newFavoriteDrink);

            return true;
        }catch (Exception var4){
            System.out.println(var4.getMessage());
            return false;
        }
    }

    public boolean addDrink(NewDrinkDto newDrinkDto){
        List<NewDrinkRecipeDto> recipeList = newDrinkDto.getRecipe();
        String drinkName = newDrinkDto.getDrinkName();
        Long id_taste = newDrinkDto.getId_taste();
        String method = newDrinkDto.getMethod();
        long result;

        try{

            List<Drink> drinkList = this.drinkRepository.findAll();
            for(Drink drink:drinkList){
                if(drink.getNameDrink().toLowerCase().equals(drinkName.toLowerCase())) return false;
            }

            result = tasteRepository.findTasteId(id_taste);
            if(result != 1) return false;

            long drink_id;

            try {
                drink_id = this.drinkRepository.getMaxId();
                drink_id++;
            }catch (Exception var4){
                drink_id = 1;
            }


            long operation_id;
            try {
                operation_id = this.recipeRepository.getMaxId();
                operation_id++;
            }catch (Exception var4){
                operation_id = 1;
            }




            for(NewDrinkRecipeDto ingredient: recipeList){
                recipeRepository.save(new Recipe(operation_id,drink_id,ingredient.getId(),ingredient.getProportion()));
                operation_id++;
            }


            drinkRepository.save(new Drink(drink_id,drinkName,method));
            drinkTasteRepository.save(new DrinkTaste(drink_id,id_taste));
            return true;
        }catch (Exception var4){
            return false;
        }
    }

}
