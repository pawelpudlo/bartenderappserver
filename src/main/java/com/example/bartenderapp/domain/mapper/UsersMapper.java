package com.example.bartenderapp.domain.mapper;

import com.example.bartenderapp.domain.dto.IdDto;
import com.example.bartenderapp.domain.dto.RangeDto;
import com.example.bartenderapp.domain.dto.UserDto;
import com.example.bartenderapp.domain.entity.Users;
import com.example.bartenderapp.domain.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class UsersMapper {


    UsersRepository usersRepository;

    @Autowired
    public UsersMapper(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public IdDto setAndGetUserId(String nick){

        nick = nick.toLowerCase();

        if(usersRepository.getNumbersNameInDataBase(nick)>0){
            return new IdDto(0);
        }else{
            long id = this.usersRepository.getMaxValueId() + 1;
            this.usersRepository.save(new Users(id,0,nick));

            return new IdDto(this.usersRepository.getUserIdByName(nick));
        }
    }

    public RangeDto getRangeDto(long id){

        try {
            return new RangeDto(this.usersRepository.getRangeById(id));
        }catch (Exception var4){
            return new RangeDto();
        }

    }

    public List<UserDto> getListUsersDto(){

        List<Users> users = (List<Users>) this.usersRepository.findAll();
        List<UserDto> usersDto = new ArrayList<>();

        for(Users user:users){
            usersDto.add(new UserDto(user.getId(),user.getName(),user.getRange()));
        }

        return usersDto;

    }

    public boolean setRange(long id){

           Optional<Users> users =  this.usersRepository.findById(id);


           if(users.isPresent()){

               String nick = users.get().getName();
               int range  = users.get().getRange();

               this.usersRepository.deleteById(id);

               if(range == 0){
                    this.usersRepository.save(new Users(id,1,nick));
                    return true;
               }else if(range == 1){
                   this.usersRepository.save(new Users(id,0,nick));
                   return true;
               }else{
                    return false;
               }

           }else{
               return false;
           }



    }

}
