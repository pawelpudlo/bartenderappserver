package com.example.bartenderapp.domain.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "recipe")
public class Recipe implements Serializable {

    @Id
    @Column(name = "operation_id")
    private long operationId;

    @Column(name = "drink_id")
    private long drinkId;


    @Column(name = "ingredient_id")
    private long ingredientId;

    @Column(name = "proportion")
    private String proportion;

    public Recipe() {
    }

    public Recipe(long operationId,long drinkId, long ingredientId, String proportion) {
        this.drinkId = drinkId;
        this.ingredientId = ingredientId;
        this.proportion = proportion;
        this.operationId = operationId;
    }

    public long getOperationId() {
        return operationId;
    }

    public void setOperationId(long operationId) {
        this.operationId = operationId;
    }

    public long getDrinkId() {
        return drinkId;
    }

    public void setDrinkId(long drinkId) {
        this.drinkId = drinkId;
    }

    public long getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(long ingredientId) {
        this.ingredientId = ingredientId;
    }

    public String getProportion() {
        return proportion;
    }

    public void setProportion(String proportion) {
        this.proportion = proportion;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "drinkId=" + drinkId +
                ", ingredientId=" + ingredientId +
                ", proportion='" + proportion + '\'' +
                '}';
    }
}
