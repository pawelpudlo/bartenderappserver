package com.example.bartenderapp.domain.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "users")
public class Users implements Serializable {

    @Id
    @Column(name = "user_id")
    private Long id;

    @Column(name = "range")
    private int range;

    @Column(name = "user_name")
    private String name;

    public Users() {
    }

    public Users(Long id,int range, String name) {
        this.id  = id;
        this.range = range;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Users{" +
                "id=" + id +
                ", range=" + range +
                ", name='" + name + '\'' +
                '}';
    }
}
