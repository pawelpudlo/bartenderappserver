package com.example.bartenderapp.domain.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "taste")
public class Taste {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "taste_id")
    private Long id;

    @Column(name = "taste")
    private String name;

    public Taste() {
    }

    public Taste(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
