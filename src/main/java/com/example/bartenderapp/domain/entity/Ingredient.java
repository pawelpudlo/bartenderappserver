package com.example.bartenderapp.domain.entity;

import javax.persistence.*;

@Entity
@Table(name = "ingredient")
public class Ingredient {

    @Id
    @Column(name = "ingredient_id")
    private long id;

    @Column(name = "ingredient_name")
    private String name;

    @Column(name = "percent")
    private int percent;

    @Column(name = "description")
    private String description;

    public Ingredient() {
    }

    public Ingredient(long id,String name, int percent, String description) {
        this.name = name;
        this.id=id;
        this.percent = percent;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", percent=" + percent +
                ", description='" + description + '\'' +
                '}';
    }
}
