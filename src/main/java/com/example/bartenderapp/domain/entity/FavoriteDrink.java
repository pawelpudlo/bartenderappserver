package com.example.bartenderapp.domain.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "favorite_drink")
public class FavoriteDrink implements Serializable {

    @Id
    @Column(name = "operation_id")
    private long id;

    @Column(name = "user_id")
    private long userId;


    @Column(name = "drink_id")
    private long drinkId;

    public FavoriteDrink() {
    }

    public FavoriteDrink(long id,long userId, long drinkId) {
        this.userId = userId;
        this.drinkId = drinkId;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getDrinkId() {
        return drinkId;
    }

    public void setDrinkId(long drinkId) {
        this.drinkId = drinkId;
    }

    @Override
    public String toString() {
        return "FavoriteDrink{" +
                "userId=" + userId +
                ", drinkId=" + drinkId +
                '}';
    }
}
