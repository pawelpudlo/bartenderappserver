package com.example.bartenderapp.domain.entity;

import javax.persistence.*;

@Entity
@Table(name = "drink")
public class Drink {

    @Id
    @Column(name = "drink_id")
    private long drinkId;

    @Column(name = "drink_name")
    private String nameDrink;

    @Column(name = "method")
    private String methodDrink;

    public Drink(){
    }

    public Drink(long drinkId,String nameDrink, String methodDrink) {
        this.nameDrink = nameDrink;
        this.methodDrink = methodDrink;
        this.drinkId = drinkId;
    }

    public long getDrinkId() {
        return drinkId;
    }

    public void setDrinkId(long drinkId) {
        this.drinkId = drinkId;
    }

    public String getNameDrink() {
        return nameDrink;
    }

    public void setNameDrink(String nameDrink) {
        this.nameDrink = nameDrink;
    }

    public String getMethodDrink() {
        return methodDrink;
    }

    public void setMethodDrink(String methodDrink) {
        this.methodDrink = methodDrink;
    }

    @Override
    public String toString() {
        return "Drink{" +
                "drinkId=" + drinkId +
                ", nameDrink='" + nameDrink + '\'' +
                ", methodDrink='" + methodDrink + '\'' +
                '}';
    }
}
