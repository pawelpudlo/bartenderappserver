package com.example.bartenderapp.domain.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "owned_alcohol")
public class OwnedAlcohol implements Serializable {

    @Id
    @Column(name = "operation_id")
    private long id;

    @Column(name = "user_id")
    private long userId;

    @Column(name = "ingredient_id")
    private long ingredientId;

    public OwnedAlcohol() {
    }

    public OwnedAlcohol(long id,long userId, long ingredientId) {
        this.userId = userId;
        this.ingredientId = ingredientId;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(long ingredientId) {
        this.ingredientId = ingredientId;
    }

    @Override
    public String toString() {
        return "OwnedAlcohol{" +
                "userId=" + userId +
                ", ingredientId=" + ingredientId +
                '}';
    }
}
