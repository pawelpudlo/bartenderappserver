package com.example.bartenderapp.domain.entity;

import javax.persistence.*;

@Entity
@Table(name = "drink_taste")
public class DrinkTaste {

    // add id

    @Id
    @Column(name = "drink_id")
    private long drinkId;

    @Column(name = "taste_id")
    private long tasteId;

    public DrinkTaste() {
    }

    public DrinkTaste(long drinkId, long tasteId) {
        this.drinkId = drinkId;
        this.tasteId = tasteId;
    }

    public long getDrinkId() {
        return drinkId;
    }

    public void setDrinkId(Long drinkId) {
        this.drinkId = drinkId;
    }

    public long getTasteId() {
        return tasteId;
    }

    public void setTasteId(long tasteId) {
        this.tasteId = tasteId;
    }


}
