package com.example.bartenderapp.domain.dto;

import java.util.List;

public class NewDrinkDto {

    private String drinkName;
    private Long id_taste;
    private List<NewDrinkRecipeDto> recipe;
    private String method;

    public NewDrinkDto(String drinkName, Long id_taste, List<NewDrinkRecipeDto> recipe, String method) {
        this.drinkName = drinkName;
        this.id_taste = id_taste;
        this.recipe = recipe;
        this.method = method;
    }

    public NewDrinkDto() {
    }

    public String getDrinkName() {
        return drinkName;
    }

    public Long getId_taste() {
        return id_taste;
    }

    public List<NewDrinkRecipeDto> getRecipe() {
        return recipe;
    }

    public String getMethod() {
        return method;
    }
}
