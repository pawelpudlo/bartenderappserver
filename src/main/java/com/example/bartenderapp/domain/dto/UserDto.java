package com.example.bartenderapp.domain.dto;

public class UserDto {

    private Long user_id;
    private String nick;
    private int range;


    public UserDto(Long user_id, String nick, int range) {
        this.user_id = user_id;
        this.nick = nick;
        this.range = range;
    }

    public Long getUser_id() {
        return user_id;
    }

    public String getNick() {
        return nick;
    }

    public int getRange() {
        return range;
    }
}
