package com.example.bartenderapp.domain.dto;

public class RecipeDto {

    private String ingredient_name;
    private String proportion;
    private int perecent;

    public RecipeDto(String ingredient_name, String proportion, int perecent) {
        this.ingredient_name = ingredient_name;
        this.proportion = proportion;
        this.perecent = perecent;
    }

    public String getIngredient_name() {
        return ingredient_name;
    }

    public String getProportion() {
        return proportion;
    }

    public int getPerecent() {
        return perecent;
    }
}
