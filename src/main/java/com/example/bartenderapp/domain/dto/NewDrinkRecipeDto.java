package com.example.bartenderapp.domain.dto;

public class NewDrinkRecipeDto {

    private Long id;
    private String proportion;

    public NewDrinkRecipeDto(Long id, String proportion) {
        this.id = id;
        this.proportion = proportion;
    }

    public NewDrinkRecipeDto() {
    }

    public Long getId() {
        return id;
    }

    public String getProportion() {
        return proportion;
    }
}
