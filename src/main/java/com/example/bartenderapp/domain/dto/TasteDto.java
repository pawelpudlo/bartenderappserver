package com.example.bartenderapp.domain.dto;

public class TasteDto {

    private Long id;
    private String name;

    public TasteDto(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
