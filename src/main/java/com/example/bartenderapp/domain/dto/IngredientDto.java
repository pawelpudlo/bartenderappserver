package com.example.bartenderapp.domain.dto;

public class IngredientDto {

    private Long ingredient_id;
    private String ingredient_name;
    private int ingredient_precent;
    private String description;

    public IngredientDto(Long ingredient_id, String ingredient_name, int ingredient_precent, String description) {
        this.ingredient_id = ingredient_id;
        this.ingredient_name = ingredient_name;
        this.ingredient_precent = ingredient_precent;
        this.description = description;
    }

    public void setIngredient_id(Long ingredient_id) {
        this.ingredient_id = ingredient_id;
    }

    public void setIngredient_name(String ingredient_name) {
        this.ingredient_name = ingredient_name;
    }

    public void setIngredient_precent(int ingredient_precent) {
        this.ingredient_precent = ingredient_precent;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getIngredient_id() {
        return ingredient_id;
    }

    public String getIngredient_name() {
        return ingredient_name;
    }

    public int getIngredient_precent() {
        return ingredient_precent;
    }

    public String getDescription() {
        return description;
    }
}
