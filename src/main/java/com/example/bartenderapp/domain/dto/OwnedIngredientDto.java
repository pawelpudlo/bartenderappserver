package com.example.bartenderapp.domain.dto;


import java.util.List;

public class OwnedIngredientDto {

    private Long userid;
    private List<IdDto> ingredients;

    public OwnedIngredientDto(Long userid, List<IdDto> ingredients) {
        this.userid = userid;
        this.ingredients = ingredients;
    }

    public OwnedIngredientDto() {
    }

    public Long getUserid(){
        return userid;
    }

    public List<IdDto> getIngredients() {
        return ingredients;
    }
}
