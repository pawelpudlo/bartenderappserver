package com.example.bartenderapp.domain.dto;

public class RangeDto {

    private int range;

    public RangeDto() {
        this.range = 3;
    }

    public RangeDto(int range) {
        this.range = range;
    }

    public int getRange() {
        return range;
    }
}
