package com.example.bartenderapp.domain.dto;

public class DrinkDto {

    private long id_drink;
    private String name_drink;
    private String taste;
    private String method;

    public DrinkDto(int id_drink, String name_drink, String taste, String method) {
        this.id_drink = id_drink;
        this.name_drink = name_drink;
        this.taste = taste;
        this.method = method;
    }

    public DrinkDto() {
    }

    public long getId_drink() {
        return id_drink;
    }

    public String getName_drink() {
        return name_drink;
    }

    public String getTaste() {
        return taste;
    }

    public void setId_drink(long id_drink) {
        this.id_drink = id_drink;
    }

    public void setName_drink(String name_drink) {
        this.name_drink = name_drink;
    }

    public void setTaste(String taste) {
        this.taste = taste;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
