package com.example.bartenderapp.domain.dto;

public class InfoDto {

    private String info;

    public InfoDto(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }
}
