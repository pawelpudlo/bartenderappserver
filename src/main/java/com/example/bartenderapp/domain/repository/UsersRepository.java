package com.example.bartenderapp.domain.repository;

import com.example.bartenderapp.domain.entity.Users;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
@Repository
public interface UsersRepository extends CrudRepository<Users,Long> {

    @Query("SELECT users.id FROM Users users WHERE users.name=:name")
    int getUserIdByName(@Param(value = "name") String name);

    @Query("SELECT COUNT(users) FROM Users users WHERE users.name=:name")
    int getNumbersNameInDataBase(@Param(value = "name") String name);

    @Query("SELECT MAX(users.id) FROM Users users")
    int getMaxValueId();

    @Query("SELECT users.range FROM Users users WHERE users.id=:id")
    int getRangeById(@Param(value = "id") long id);

}
