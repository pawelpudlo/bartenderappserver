package com.example.bartenderapp.domain.repository;

import com.example.bartenderapp.domain.entity.Ingredient;
import com.example.bartenderapp.domain.entity.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IngredientRepository extends JpaRepository<Ingredient,Long> {

    @Query("SELECT ingredient FROM Ingredient ingredient, OwnedAlcohol ownedAlcohol, Users users WHERE users.id=:userId AND users.id=ownedAlcohol.userId AND ownedAlcohol.ingredientId=ingredient.id")
    List<Ingredient> getOwnedIngredientByUserId(@Param(value = "userId") long userId);

    @Query("SELECT ingredient FROM Recipe recipe, Ingredient ingredient WHERE ingredient.id=recipe.ingredientId AND recipe.ingredientId=:id")
    Ingredient getDrinkIngredientByDrinkId(@Param("id") long id);



    @Query("SELECT MAX(ingredient.id) FROM Ingredient ingredient")
    long getMaxOperationId();
}

