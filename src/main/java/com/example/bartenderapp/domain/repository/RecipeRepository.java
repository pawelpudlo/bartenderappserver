package com.example.bartenderapp.domain.repository;

import com.example.bartenderapp.domain.entity.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe,Long> {

    @Query("SELECT recipe FROM Recipe recipe WHERE recipe.drinkId=:drinkId")
    List<Recipe> getDrinkRecipe(@Param("drinkId") long drinkId);

    @Query("SELECT MAX(recipe.operationId) FROM Recipe recipe")
    long getMaxId();

}
