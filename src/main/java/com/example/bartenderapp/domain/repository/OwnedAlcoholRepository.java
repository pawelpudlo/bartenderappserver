package com.example.bartenderapp.domain.repository;

import com.example.bartenderapp.domain.entity.OwnedAlcohol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OwnedAlcoholRepository extends JpaRepository<OwnedAlcohol,Long> {

    @Query("SELECT ownedAlcohol.id FROM OwnedAlcohol ownedAlcohol WHERE ownedAlcohol.userId=:user_id AND ownedAlcohol.ingredientId=:ingredient_id")
    long getIdOperationOwnedAlcoholByUserIdAndDrinkId(@Param(value = "user_id") long user_id,
                                                       @Param(value = "ingredient_id") long ingredient_id);

    @Query("SELECT COUNT(ownedAlcohol) FROM OwnedAlcohol ownedAlcohol WHERE ownedAlcohol.userId=:user_id AND ownedAlcohol.ingredientId=:ingredient_id")
    long getNumberOfAppearancesInTable(@Param(value = "user_id") long user_id,
                                                      @Param(value = "ingredient_id") long ingredient_id);

    @Query("SELECT MAX(ow.id) FROM OwnedAlcohol ow")
    long getMaxOperationId();



}
