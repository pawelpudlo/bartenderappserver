package com.example.bartenderapp.domain.repository;

import com.example.bartenderapp.domain.dto.DrinkDto;
import com.example.bartenderapp.domain.entity.Drink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DrinkRepository extends JpaRepository<Drink,Long> {



    @Query("SELECT d FROM Drink d")
    List<Drink> findAllDrinkAndHimTaste();

    @Query("SELECT drink FROM Drink drink, FavoriteDrink  favoriteDrink, Users users WHERE users.id=:userId AND users.id=favoriteDrink.userId AND favoriteDrink.drinkId=drink.drinkId")
    List<Drink> getListFavoriteDrinkByUserId(@Param("userId") long userId);

    @Query("SELECT drink FROM Drink drink, Recipe recipe, Ingredient ingredient, OwnedAlcohol o WHERE o.userId=:userId AND ingredient.id=o.ingredientId AND ingredient.id=recipe.ingredientId and recipe.drinkId=drink.drinkId")
    List<Drink> getListDrinkWithOwnedIngredients(@Param("userId") long userId);

    @Query("SELECT MAX(drink.drinkId) FROM Drink drink")
    long getMaxId();

}
