package com.example.bartenderapp.domain.repository;

import com.example.bartenderapp.domain.entity.DrinkTaste;
import com.example.bartenderapp.domain.entity.Taste;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TasteRepository extends JpaRepository<Taste,Long> {

    @Query("select t.name from Taste as t WHERE t.id = :tasteId")
    String findTasteNameById(@Param("tasteId")Long id);


    @Query("select COUNT(t.id) from Taste as t WHERE t.id = :tasteId")
    long findTasteId(@Param("tasteId") Long tasteId);

}
