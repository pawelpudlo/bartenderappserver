package com.example.bartenderapp.domain.repository;

import com.example.bartenderapp.domain.entity.Drink;
import com.example.bartenderapp.domain.entity.DrinkTaste;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DrinkTasteRepository extends JpaRepository<DrinkTaste,Integer> {

    @Query("select dt.tasteId from DrinkTaste as dt WHERE dt.drinkId = :drinkId")
    Long findTasteById(@Param("drinkId")Long drinkId);

}
