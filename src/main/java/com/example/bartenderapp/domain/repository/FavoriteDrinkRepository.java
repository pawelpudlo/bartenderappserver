package com.example.bartenderapp.domain.repository;


import com.example.bartenderapp.domain.entity.Drink;
import com.example.bartenderapp.domain.entity.FavoriteDrink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FavoriteDrinkRepository extends JpaRepository<FavoriteDrink,Long> {

    @Query("SELECT favoriteDrink.id FROM FavoriteDrink favoriteDrink WHERE favoriteDrink.drinkId=:drink_id AND favoriteDrink.userId=:user_id")
    long getIdOperationFavoriteDrinkByUserIdAndDrinkId(@Param(value = "user_id") long user_id,
                                                                      @Param(value = "drink_id") long drink_id);

    @Query("SELECT COUNT(favoriteDrink)  FROM FavoriteDrink favoriteDrink WHERE favoriteDrink.drinkId=:drink_id AND favoriteDrink.userId=:user_id")
    long getNumberOfAppearancesInTable(@Param(value = "user_id") long user_id,
                                                       @Param(value = "drink_id") long drink_id);

    @Query("SELECT MAX(fd.id) FROM FavoriteDrink fd")
    long getMaxOperationId();



}
