package com.example.bartenderapp.service.serviceImpl;

import com.example.bartenderapp.domain.dto.*;
import com.example.bartenderapp.domain.entity.Drink;
import com.example.bartenderapp.domain.mapper.DrinkListMapper;
import com.example.bartenderapp.domain.mapper.IngredientMapper;
import com.example.bartenderapp.domain.mapper.TasteMapper;
import com.example.bartenderapp.domain.mapper.UsersMapper;
import com.example.bartenderapp.domain.repository.DrinkRepository;
import com.example.bartenderapp.service.BartenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BartenderServiceImpl implements BartenderService {

    private final DrinkRepository drinkRepository;
    private final DrinkListMapper drinkListMapper;
    private final UsersMapper usersMapper;
    private final IngredientMapper ingredientMapper;
    private final TasteMapper tasteMapper;

    @Autowired
    public BartenderServiceImpl(DrinkRepository drinkRepository,
                                DrinkListMapper drinkListMapper,
                                UsersMapper usersMapper,
                                IngredientMapper ingredientMapper,
                                TasteMapper tasteMapper) {
        this.drinkRepository = drinkRepository;
        this.drinkListMapper = drinkListMapper;
        this.usersMapper = usersMapper;
        this.ingredientMapper = ingredientMapper;
        this.tasteMapper = tasteMapper;
    }

    @Override
    public List<DrinkDto> getAllDrinks() {
        List<Drink> drinks = drinkRepository.findAllDrinkAndHimTaste();
        return drinkListMapper.mapToDto(drinks);
    }


    public IdDto setUserId(String nick){
        return this.usersMapper.setAndGetUserId(nick);
    }

    @Override
    public RangeDto getRange(long id) {
        return this.usersMapper.getRangeDto(id);
    }

    @Override
    public List<UserDto> getUsers() {
        return this.usersMapper.getListUsersDto();
    }

    @Override
    public Boolean setRange(long id) {
        return this.usersMapper.setRange(id);
    }

    @Override
    public List<DrinkDto> getFavoriteDrink(long id) {
        return this.drinkListMapper.getListFavoriteDrink(id);
    }

    @Override
    public List<IngredientDto> getOwnedIngredient(long id) {
        return this.ingredientMapper.getListWithOwnedIngredientInDto(id);
    }

    @Override
    public List<TasteDto> getTaste() {
        return this.tasteMapper.getListTastDto();
    }

    @Override
    public List<IngredientDto> getIngredients() {
        return this.ingredientMapper.getAllIngredients();
    }

    @Override
    public List<RecipeDto> getDrinkRecipe(long id) {
        return drinkListMapper.getDrinkRecipe(id);
    }

    @Override
    public List<DrinkDto> getDrinksWithOwnedIngredient(long id) {

        return drinkListMapper.getDrinkWithOwnedIngredientInDto(id);
    }

    @Override
    public boolean deleteFavoriteDrink(long user_id, long drink_id) {
        return this.drinkListMapper.deleteFavoriteDrink(user_id,drink_id);
    }

    @Override
    public boolean deleteOwnedIngredient(long user_id, long ingredient_id) {
        return this.ingredientMapper.deleteOwnedIngredient(user_id,ingredient_id);
    }

    @Override
    public boolean addFavoriteDrink(long user_id, long drink_id) {

        return this.drinkListMapper.addFavoriteDrink(user_id,drink_id);
    }

    @Override
    public boolean addOwnedIngredient(OwnedIngredientDto ownedIngredientDto) {
        return this.ingredientMapper.addOwnedIngredient(ownedIngredientDto);
    }

    @Override
    public boolean addIngredient(String name, int percent, String description) {
        return this.ingredientMapper.addIngredient(name,percent,description);
    }

    @Override
    public boolean addDrink(NewDrinkDto newDrinkDto) {

        return this.drinkListMapper.addDrink(newDrinkDto);
    }


}
