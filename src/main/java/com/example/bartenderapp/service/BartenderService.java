package com.example.bartenderapp.service;

import com.example.bartenderapp.domain.dto.*;

import java.util.List;

public interface BartenderService {

    List<DrinkDto> getAllDrinks();

    IdDto setUserId(String nick);

    RangeDto getRange(long id);

    List<UserDto> getUsers();

    Boolean setRange(long id);

    List<DrinkDto> getFavoriteDrink(long id);

    List<IngredientDto> getOwnedIngredient(long id);

    List<TasteDto> getTaste();

    List<IngredientDto> getIngredients();

    List<RecipeDto> getDrinkRecipe(long id);

    List<DrinkDto> getDrinksWithOwnedIngredient(long id);

    boolean deleteFavoriteDrink(long user_id, long drink_id);

    boolean deleteOwnedIngredient(long user_id, long drink_id);

    boolean addFavoriteDrink(long user_id, long drink_id);

    boolean addOwnedIngredient(OwnedIngredientDto ownedIngredientDto);

    boolean addIngredient(String name, int percent ,String description);

    boolean addDrink(NewDrinkDto newDrinkDto);


}
