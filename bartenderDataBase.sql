PGDMP                         y           postgres    12.2    12.2 0    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    13318    postgres    DATABASE     �   CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Polish_Poland.1250' LC_CTYPE = 'Polish_Poland.1250';
    DROP DATABASE postgres;
                postgres    false            �           0    0    DATABASE postgres    COMMENT     N   COMMENT ON DATABASE postgres IS 'default administrative connection database';
                   postgres    false    3010            
            2615    16393    pgagent    SCHEMA        CREATE SCHEMA pgagent;
    DROP SCHEMA pgagent;
                postgres    false            �           0    0    SCHEMA pgagent    COMMENT     6   COMMENT ON SCHEMA pgagent IS 'pgAgent system tables';
                   postgres    false    10                        3079    16384 	   adminpack 	   EXTENSION     A   CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;
    DROP EXTENSION adminpack;
                   false            �           0    0    EXTENSION adminpack    COMMENT     M   COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';
                        false    1                        3079    16394    pgagent 	   EXTENSION     <   CREATE EXTENSION IF NOT EXISTS pgagent WITH SCHEMA pgagent;
    DROP EXTENSION pgagent;
                   false    10            �           0    0    EXTENSION pgagent    COMMENT     >   COMMENT ON EXTENSION pgagent IS 'A PostgreSQL job scheduler';
                        false    3            �            1255    49396    dodaj(real, real, real)    FUNCTION     �   CREATE FUNCTION public.dodaj(x real, y real, z real) RETURNS real
    LANGUAGE plpgsql STRICT
    AS $$
BEGIN
RAISE NOTICE 'Dodajemy!';
RETURN x + y + z;
END
$$;
 4   DROP FUNCTION public.dodaj(x real, y real, z real);
       public          postgres    false            �            1255    98818 %   update_ingredient_statistic_percent()    FUNCTION       CREATE FUNCTION public.update_ingredient_statistic_percent() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
IF (TG_OP = 'UPDATE') THEN
DELETE FROM public.liquor_statistic WHERE ingredient_id = new.ingredient_id;
INSERT INTO public.liquor_statistic
SELECT OLD.ingredient_id, (select count(*) from owned_alcohol where ingredient_id=old.ingredient_id) ,
(select count(*) from owned_alcohol where ingredient_id=old.ingredient_id)::float*100/(SELECT COUNT(user_id) FROM users)::float,now();

END IF; 
RETURN NULL;
END;
$$;
 <   DROP FUNCTION public.update_ingredient_statistic_percent();
       public          postgres    false            �            1255    98805    update_precent_drinks()    FUNCTION     n  CREATE FUNCTION public.update_precent_drinks() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'UPDATE') THEN
DELETE FROM public.drink_statistic WHERE drink_id = new.drink_id;
INSERT INTO public.drink_statistic
SELECT OLD.drink_id, NEW.quantity ,ROUND(NEW.quantity*100/(SELECT COUNT(user_id) FROM favorite_drink),2);

END IF; 
RETURN NULL;
END;
$$;
 .   DROP FUNCTION public.update_precent_drinks();
       public          postgres    false            �            1255    98816    update_statistic_drinks()    FUNCTION     K  CREATE FUNCTION public.update_statistic_drinks() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'DELETE') THEN
DELETE FROM public.drink_statistic WHERE old.drink_id = new.drink_id;
INSERT INTO public.drink_statistic
values (new.drink_id, ROUND((SELECT COUNT(*) FROM favorite_drink where drink_id=new.drink_id),2) ,ROUND((SELECT COUNT(user_id) FROM favorite_drink where drink_id=new.drink_id)*100/(SELECT COUNT(user_id) FROM users),2));

ELSIF (TG_OP = 'INSERT') THEN
DELETE FROM public.drink_statistic WHERE old.drink_id = new.drink_id;
INSERT INTO public.drink_statistic
values (new.drink_id, ROUND((SELECT COUNT(*) FROM favorite_drink where drink_id=old.drink_id),2) , ROUND((SELECT COUNT(user_id) FROM favorite_drink where drink_id=new.drink_id)*100/(SELECT COUNT(user_id) FROM users),2));

END IF; 
RETURN NULL;
END;
$$;
 0   DROP FUNCTION public.update_statistic_drinks();
       public          postgres    false            �            1255    98826 	   userlog()    FUNCTION     j  CREATE FUNCTION public.userlog() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF (TG_OP = 'UPDATE') THEN
INSERT INTO public.user_log
SELECT old.user_id, (select range from users where user_id=old.user_id) ,old.range,now();

ELSIF (TG_OP = 'INSERT') then
INSERT INTO public.user_log
SELECT new.user_id, 0 ,old.range,now();

END IF; 
RETURN NULL;
END;
$$;
     DROP FUNCTION public.userlog();
       public          postgres    false            �            1259    123484    drink    TABLE     �   CREATE TABLE public.drink (
    drink_id bigint NOT NULL,
    drink_name character varying(255) NOT NULL,
    method character varying(255)
);
    DROP TABLE public.drink;
       public         heap    postgres    false            �            1259    123460    drink_taste    TABLE     `   CREATE TABLE public.drink_taste (
    drink_id bigint NOT NULL,
    taste_id bigint NOT NULL
);
    DROP TABLE public.drink_taste;
       public         heap    postgres    false            �            1259    123455    favorite_drink    TABLE     {   CREATE TABLE public.favorite_drink (
    operation_id bigint NOT NULL,
    user_id bigint NOT NULL,
    drink_id bigint
);
 "   DROP TABLE public.favorite_drink;
       public         heap    postgres    false            �            1259    123447 
   ingredient    TABLE     �   CREATE TABLE public.ingredient (
    ingredient_id bigint NOT NULL,
    ingredient_name character varying(255) NOT NULL,
    percent numeric NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public.ingredient;
       public         heap    postgres    false            �            1259    123479    owned_alcohol    TABLE        CREATE TABLE public.owned_alcohol (
    operation_id bigint NOT NULL,
    user_id bigint NOT NULL,
    ingredient_id bigint
);
 !   DROP TABLE public.owned_alcohol;
       public         heap    postgres    false            �            1259    123437    recipe    TABLE     �   CREATE TABLE public.recipe (
    operation_id bigint NOT NULL,
    drink_id bigint NOT NULL,
    ingredient_id bigint NOT NULL,
    proportion character varying(255) NOT NULL
);
    DROP TABLE public.recipe;
       public         heap    postgres    false            �            1259    98765    taste    TABLE     g   CREATE TABLE public.taste (
    taste_id bigint NOT NULL,
    taste character varying(255) NOT NULL
);
    DROP TABLE public.taste;
       public         heap    postgres    false            �            1259    123432    users    TABLE     �   CREATE TABLE public.users (
    user_id bigint NOT NULL,
    range integer NOT NULL,
    user_name character varying(255) NOT NULL
);
    DROP TABLE public.users;
       public         heap    postgres    false            �
          0    16395    pga_jobagent 
   TABLE DATA           I   COPY pgagent.pga_jobagent (jagpid, jaglogintime, jagstation) FROM stdin;
    pgagent          postgres    false    207   =7       �
          0    16406    pga_jobclass 
   TABLE DATA           7   COPY pgagent.pga_jobclass (jclid, jclname) FROM stdin;
    pgagent          postgres    false    209   �7       �
          0    16418    pga_job 
   TABLE DATA           �   COPY pgagent.pga_job (jobid, jobjclid, jobname, jobdesc, jobhostagent, jobenabled, jobcreated, jobchanged, jobagentid, jobnextrun, joblastrun) FROM stdin;
    pgagent          postgres    false    211   �7       �
          0    16470    pga_schedule 
   TABLE DATA           �   COPY pgagent.pga_schedule (jscid, jscjobid, jscname, jscdesc, jscenabled, jscstart, jscend, jscminutes, jschours, jscweekdays, jscmonthdays, jscmonths) FROM stdin;
    pgagent          postgres    false    215   �7       �
          0    16500    pga_exception 
   TABLE DATA           J   COPY pgagent.pga_exception (jexid, jexscid, jexdate, jextime) FROM stdin;
    pgagent          postgres    false    217   �7       �
          0    16515 
   pga_joblog 
   TABLE DATA           X   COPY pgagent.pga_joblog (jlgid, jlgjobid, jlgstatus, jlgstart, jlgduration) FROM stdin;
    pgagent          postgres    false    219   8       �
          0    16444    pga_jobstep 
   TABLE DATA           �   COPY pgagent.pga_jobstep (jstid, jstjobid, jstname, jstdesc, jstenabled, jstkind, jstcode, jstconnstr, jstdbname, jstonerror, jscnextrun) FROM stdin;
    pgagent          postgres    false    213   8       �
          0    16532    pga_jobsteplog 
   TABLE DATA           |   COPY pgagent.pga_jobsteplog (jslid, jsljlgid, jsljstid, jslstatus, jslresult, jslstart, jslduration, jsloutput) FROM stdin;
    pgagent          postgres    false    221   ;8       �          0    123484    drink 
   TABLE DATA           =   COPY public.drink (drink_id, drink_name, method) FROM stdin;
    public          postgres    false    229   X8       �          0    123460    drink_taste 
   TABLE DATA           9   COPY public.drink_taste (drink_id, taste_id) FROM stdin;
    public          postgres    false    227   �8       �          0    123455    favorite_drink 
   TABLE DATA           I   COPY public.favorite_drink (operation_id, user_id, drink_id) FROM stdin;
    public          postgres    false    226   �8       �          0    123447 
   ingredient 
   TABLE DATA           Z   COPY public.ingredient (ingredient_id, ingredient_name, percent, description) FROM stdin;
    public          postgres    false    225   �8       �          0    123479    owned_alcohol 
   TABLE DATA           M   COPY public.owned_alcohol (operation_id, user_id, ingredient_id) FROM stdin;
    public          postgres    false    228   ,9       �          0    123437    recipe 
   TABLE DATA           S   COPY public.recipe (operation_id, drink_id, ingredient_id, proportion) FROM stdin;
    public          postgres    false    224   S9       �          0    98765    taste 
   TABLE DATA           0   COPY public.taste (taste_id, taste) FROM stdin;
    public          postgres    false    222   9       �          0    123432    users 
   TABLE DATA           :   COPY public.users (user_id, range, user_name) FROM stdin;
    public          postgres    false    223   �9       6           2606    123491    drink drink_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.drink
    ADD CONSTRAINT drink_pkey PRIMARY KEY (drink_id);
 :   ALTER TABLE ONLY public.drink DROP CONSTRAINT drink_pkey;
       public            postgres    false    229            2           2606    123464    drink_taste drink_taste_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.drink_taste
    ADD CONSTRAINT drink_taste_pkey PRIMARY KEY (drink_id);
 F   ALTER TABLE ONLY public.drink_taste DROP CONSTRAINT drink_taste_pkey;
       public            postgres    false    227            0           2606    123459 "   favorite_drink favorite_drink_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.favorite_drink
    ADD CONSTRAINT favorite_drink_pkey PRIMARY KEY (operation_id);
 L   ALTER TABLE ONLY public.favorite_drink DROP CONSTRAINT favorite_drink_pkey;
       public            postgres    false    226            .           2606    123454    ingredient ingredient_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY public.ingredient
    ADD CONSTRAINT ingredient_pkey PRIMARY KEY (ingredient_id);
 D   ALTER TABLE ONLY public.ingredient DROP CONSTRAINT ingredient_pkey;
       public            postgres    false    225            4           2606    123483     owned_alcohol owned_alcohol_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.owned_alcohol
    ADD CONSTRAINT owned_alcohol_pkey PRIMARY KEY (operation_id);
 J   ALTER TABLE ONLY public.owned_alcohol DROP CONSTRAINT owned_alcohol_pkey;
       public            postgres    false    228            ,           2606    123441    recipe recipe_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.recipe
    ADD CONSTRAINT recipe_pkey PRIMARY KEY (operation_id);
 <   ALTER TABLE ONLY public.recipe DROP CONSTRAINT recipe_pkey;
       public            postgres    false    224            (           2606    98834    taste taste_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.taste
    ADD CONSTRAINT taste_pkey PRIMARY KEY (taste_id);
 :   ALTER TABLE ONLY public.taste DROP CONSTRAINT taste_pkey;
       public            postgres    false    222            *           2606    123436    users users_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    223            �
   @   x�3742�4202�50�54W02�20�21�347��4�60��q���	0�26����� =�d      �
      x������ � �      �
      x������ � �      �
      x������ � �      �
      x������ � �      �
      x������ � �      �
      x������ � �      �
      x������ � �      �   9   x�3��O�NT�Rp��9���_�����`d`���L�i�E
�`1��=... g�w      �      x�3�4����� ]      �      x�3�4�4����� �X      �   7   x�3�t�ONTp��I�4��/�L��K�Q�rq�ޜ���ib�fq��qqq �Hb      �      x�3�4�4�2�F\1z\\\ 	      �      x�3�4B#.#�ij����� 'T      �   '   x�3�t�,)I-�2���/�M��2�,.OM-����� ��      �      x�3�4�LL�������� �1     